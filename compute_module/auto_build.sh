#!/bin/bash

echo ""
echo "PROJ_BASE_DIR = $PROJ_BASE_DIR"
echo "BUILDROOT_BASE = $BUILDROOT_BASE"
echo "CONFIG_BASE_DIR = $CONFIG_BASE_DIR"
echo "KERNEL_CONFIG = $CM_KERNEL_CONFIG"
echo ""

# Validate that this is indeed the base directory
if [ ! -d $BUILDROOT_BASE ];
then
	echo "Cannot find directory $BUILDROOT_BASE"
	echo "This does not seem to be the project base directory"
	exit
elif [ ! -d $CONFIG_BASE_DIR ];
then
	echo "Cannot find directory $CONFIG_BASE_DIR"
	echo "This does not seem to be the project base directory"
	exit
elif [ ! -d $OPENCV_APP_DIR ];
then
	echo "Cannot find directory $OPENCV_APP_DIR"
	echo "This does not seem to be the project base directory"
	exit
fi

cd $BUILDROOT_BASE

if [ -f $CM_BUILDROOT_CONFIG ];
then
	diff_config=`diff .config $CM_BUILDROOT_CONFIG`
	if [ "$diff_config" != "" ];
	then
		echo "Buildroot config already set..."
	else
		echo "Building buildroot configuration..."
		make compute_module_defconfig || exit 1
	fi
else
	echo "Cannot locate $CM_BUILDROOT_CONFIG. Using raspberrypi_defconfig"
	make raspberrypi_defconfig || exit 1
fi

if [ ! -f "$CM_KERNEL_CONFIG" ];
then
	echo "Cannot find kernel configuration file!!!!"
	echo "The buildroot is set to use this config file."
	echo "Execute \"make menuconfig\" in the $BUILDROOT_BASE directory and change the kernel config selection"
fi

make || exit 1

cd $OPENCV_APP_DIR

# Build our set of openCV applications and copy to rootfs
if [ -f "build_opencv_app.sh" ];
then
	./build_opencv_app.sh
else
	echo "Cannot find build_opencv_app.sh in $OPENCV_APP_DIR"
	echo "You will need to compile any opencv applications manually and copy them to the rootfs"
fi

cd $PROJ_BASE_DIR

echo ""
echo "Build complete. Please find the image to be flashed at $BUILDROOT_BASE/output/images/sdcard.img"
echo " "
