The components for this directory:

1. bcm_usbboot - Buildroot and BCM boot-up utilities
2. documentation
3. reference_files_and_configs - Backup of the bootloader config, kernel config, buildroot config and working images.

To perform an auto build: Run

	> source build_env.sh
	> ./auto_build.sh
