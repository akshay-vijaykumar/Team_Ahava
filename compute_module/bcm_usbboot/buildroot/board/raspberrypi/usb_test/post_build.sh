#!/bin/bash

mkdir -p output/sd_card
cp output/build/rpi-firmware-*/boot/start_cd.elf output/sd_card/start.elf
cp output/build/rpi-firmware-*/boot/bootcode.bin output/sd_card/
echo "ramfsfile=rootfs" > output/sd_card/config.txt
echo "ramfsaddr=0xffffffff" >> output/sd_card/config.txt
cp output/images/zImage output/sd_card/kernel.img
cp output/images/rootfs.cpio.lzo output/sd_card/rootfs

FSIZE=`du output/sd_card/ | cut -f 1`
dd if=/dev/zero of=output/images/fatimage bs=1KiB count=$(($FSIZE+20))
sudo losetup /dev/loop0 output/images/fatimage
sudo mkfs.vfat /dev/loop0
mkdir -p output/tmp
sudo mount /dev/loop0 output/tmp
sudo cp output/sd_card/kernel.img output/tmp
sudo cp output/sd_card/rootfs output/tmp
sudo cp output/sd_card/config.txt output/tmp
sudo umount /dev/loop0
sudo losetup -d /dev/loop0
