=============================================
Schematics File information

=============================================

1. CM Rev 1.1 		- Schematics for the compute module
2. CMIO Rev 1.2 	- Schematics for the compute module IO board ( CMIO )
3. CMCDA Rev 1.1 	- Schematics for the Compute Module camera/Display adapter board ( CMCDA )

