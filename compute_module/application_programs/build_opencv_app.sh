#!/bin/bash

echo ""
echo "Building opencv applications........"

ROOTFS_DIR=$BUILDROOT_BASE/output/target/
CM_CMAKE=$BUILDROOT_BASE/output/host/usr/bin/cmake 
CM_CMAKE_TOOLCHAIN=$BUILDROOT_BASE/output/host/usr/share/buildroot/toolchainfile.cmake

if [ ! -d $ROOTFS_DIR ];
then
	echo "Cannot locate $ROOTFS_DIR"
	echo "Buildroot is probably incomplete"
	echo "Please run $PROJ_BASE_DIR/build_env.sh"
	exit
fi

# Rootfs directory found. Mount it
for file in ./*/
do
	( 
	  cd $file
	  pwd 

	  if [ -f "CMakeLists.txt" ];
	  then
		$CM_CMAKE -DCMAKE_TOOLCHAIN_FILE=$CM_CMAKE_TOOLCHAIN
		app_target=`make | grep "Built target"`
		target_app=`echo $app_target | cut -d " " -f 4`
		if [ "$target_app" != "" ];
		then
			echo "Copying $target_app to rootfs"
			sudo cp $target_app $ROOTFS_DIR/usr/bin/
		fi

		
	  fi
	)
done

# Run target-post-image
cd $BUILDROOT_BASE
make target-post-image
