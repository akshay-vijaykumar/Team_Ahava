#include <string>
#include <iostream>
#include <sstream>
#include <sched.h>
#include <sys/resource.h>
#include <unistd.h>
#include <fcntl.h>

#include <opencv2/core/utility.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include "img_uart_comm.h"

using namespace cv;
using namespace std;

#define TRACKBAR 0
#define UART_ENABLE 1

#if UART_ENABLE
#define UART_DEV_PATH "/dev/ttyS0"
bool sendObjectInfo (unsigned int lArea, unsigned int lX, unsigned int lY);
#endif

//initial min and max HSV filter values.
//these will be changed using trackbars
#define MAXVAL 256

// Window Enabling
#define HSV_WINDOW 0
#define THRESHOLD_WINDOW 0

bool thresholdWindowStatus = false;
string filterValueFile;
int filterFileID;

#if 1 // Values based on Testing - Green Cup
	
	int H_MIN = 73;
	int H_MAX = 89;

	int S_MIN = 90;
	int S_MAX = 154;

	int V_MIN = 50;
	int V_MAX = 117;

#else

	int H_MIN = 0;
	int H_MAX = 256;

	int S_MIN = 0;
	int S_MAX = 256;

	int V_MIN = 0;
	int V_MAX = 256;

#endif

typedef struct filterValues{
	unsigned int hmin;
	unsigned int hmax;
	unsigned int smin;
	unsigned int smax;
	unsigned int vmin;
	unsigned int vmax;
} filterValues_t;

bool trackObjects = true;
bool useMorphOps = true;

//default capture width and height
#if 0
const int FRAME_WIDTH = 480;
const int FRAME_HEIGHT = 320;
#else
const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;
#endif

const int FRAME_RATE = 2;

//max number of objects to be detected in frame
const int MAX_NUM_OBJECTS = 75;

//minimum and maximum object area
const int MIN_OBJECT_AREA = 20 * 20;
const int MAX_OBJECT_AREA = FRAME_HEIGHT*FRAME_WIDTH/1.5;

double refArea = 0;

//names that will appear at the top of each window
const string ActualWindow = "Original Image";
const string HSVImageWindow = "HSV Image";
const string ThresholdWindow = "Thresholded Image";

const string ActualWindow3 = "After Morphological Operations";
const string trackbarActualWindow = "Trackbars";

#define CMD_HMIN_INCR 'q'
#define CMD_HMIN_DECR 'a'
#define CMD_HMAX_INCR 'w'
#define CMD_HMAX_DECR 's'

#define CMD_SMIN_INCR 'e'
#define CMD_SMIN_DECR 'd'
#define CMD_SMAX_INCR 'r'
#define CMD_SMAX_DECR 'f'

#define CMD_VMIN_INCR 't'
#define CMD_VMIN_DECR 'g'
#define CMD_VMAX_INCR 'y'
#define CMD_VMAX_DECR 'h'

#define CMD_WINDOW_TH_IMAGE 'p'
#define CMD_LOAD_FILTER_VALUES 'l'
#define CMD_STORE_FILTER_VALUES 'k'
#define CMD_RESET_FITLER 'v'

#define CMD_ESC (27)

//x and y values for the location of the object
int x = 0, y = 0;


void on_trackbar( int, void* )
{
	//This function gets called whenever a
	// trackbar position is changed

}

string intToString(int number)
{
	std::stringstream ss;
	ss << number;
	return ss.str();
}

#if TRACKBAR
void createTrackbars()
{
	//create memory to store trackbar name on window
	char TrackbarName[50];

	//create window for trackbars
    namedWindow(trackbarActualWindow,0);


	sprintf( TrackbarName, "H_MIN", H_MIN);
	sprintf( TrackbarName, "H_MAX", H_MAX);
	sprintf( TrackbarName, "S_MIN", S_MIN);
	sprintf( TrackbarName, "S_MAX", S_MAX);
	sprintf( TrackbarName, "V_MIN", V_MIN);
	sprintf( TrackbarName, "V_MAX", V_MAX);

	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.H_LOW),
	//the max value the trackbar can move (eg. H_HIGH), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbar)
	//                                  ---->    ---->     ---->      
    createTrackbar( "H_MIN", trackbarActualWindow, &H_MIN, H_MAX, on_trackbar );
    createTrackbar( "H_MAX", trackbarActualWindow, &H_MAX, H_MAX, on_trackbar );
    createTrackbar( "S_MIN", trackbarActualWindow, &S_MIN, S_MAX, on_trackbar );
    createTrackbar( "S_MAX", trackbarActualWindow, &S_MAX, S_MAX, on_trackbar );
    createTrackbar( "V_MIN", trackbarActualWindow, &V_MIN, V_MAX, on_trackbar );
    createTrackbar( "V_MAX", trackbarActualWindow, &V_MAX, V_MAX, on_trackbar );

}
#endif

void drawObject(int x, int y,Mat &frame)
{

	//use some of the openCV drawing functions to draw crosshairs
	//on your tracked image!

    //UPDATE:JUNE 18TH, 2013
    //added 'if' and 'else' statements to prevent
    //memory errors from writing off the screen (ie. (-25,-25) is not within the window!)

	circle(frame, Point(x,y), 20, Scalar(0,255,0), 2);

    if( (y - 25) > 0 )
    {
    	line(frame, Point(x,y), Point(x,y-25), Scalar(0,255,0), 2);
    }
    else 
    {	
    	line(frame, Point(x,y), Point(x,0), Scalar(0,255,0), 2);
    }

    if( (y + 25) < FRAME_HEIGHT)
    {
  	 	line(frame, Point(x,y), Point(x,y+25), Scalar(0,255,0), 2);
	}	
    else 
	{
		line(frame, Point(x,y), Point(x,FRAME_HEIGHT), Scalar(0,255,0), 2);
	}
    
    if((x - 25) > 0)
    {
    	line(frame, Point(x,y), Point(x-25,y), Scalar(0,255,0), 2);
    }
    else 
	{
		line(frame, Point(x,y), Point(0,y), Scalar(0,255,0), 2);
    }

    if((x + 25) < FRAME_WIDTH)
    {
    	line(frame, Point(x,y), Point(x+25,y), Scalar(0,255,0), 2);
    }
    else 
    {
    	line(frame, Point(x,y), Point(FRAME_WIDTH,y), Scalar(0,255,0), 2);
    }

	putText(frame,intToString(x)+","+intToString(y),Point(x,y+30),1,1,Scalar(0,255,0),2);

}

void morphOps(Mat &thresh)
{

	//create structuring element that will be used to "dilate" and "erode" image.
	//the element chosen here is a 3px by 3px rectangle
#if 1
	static int erodeElementSize = 3;
	static int dilateElementSize = 8;
#else
	static int erodeElementSize = 1;
	static int dilateElementSize = 3;
#endif

	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(erodeElementSize, erodeElementSize));

    //dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(dilateElementSize, dilateElementSize));

	//erode(thresh,thresh,erodeElement);
	erode(thresh,thresh,erodeElement);

	//dilate(thresh,thresh,dilateElement);
	dilate(thresh,thresh,dilateElement);

	return;
	
}

void trackFilteredObject(int &lx, int &ly, Mat threshold, Mat &cameraFeed)
{

	Mat temp;
	threshold.copyTo(temp);

	//these two vectors needed for output of findContours
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//find contours of filtered image using openCV findContours function
	findContours(temp,contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

	//use moments method to find our filtered object
	refArea = 0;
	bool objectFound = false;

	if (hierarchy.size() > 0) 
	{
		int numObjects = hierarchy.size();

        //if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
        if(numObjects < MAX_NUM_OBJECTS)
        {
			for (int index = 0; index >= 0; index = hierarchy[index][0]) 
			{

				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;

				//if the area is less than 20 px by 20px then it is probably just noise
				//if the area is the same as the 3/2 of the image size, probably just a bad filter
				//we only want the object with the largest area so we safe a reference area each
				//iteration and compare it to the area in the next iteration.
                if((area > MIN_OBJECT_AREA) && (area < MAX_OBJECT_AREA) && (area > refArea))
                {
					lx = moment.m10/area;
					ly = moment.m01/area;
					refArea = area;
					objectFound = true;

				}
				else 
				{
					refArea = 0;
					objectFound = false;
				}

			}
			//let user know you found an object
			if(objectFound == true)
			{
				putText(cameraFeed, "Tracking Object", Point(0,50), 2, 1, Scalar(0,255,0), 2);

				//draw object location on screen
				drawObject(lx, ly, cameraFeed);
			}
		}
		else 
		{

			putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0,50), 1, 2, Scalar(0,0,255), 2);
		}
	}

#if UART_ENABLE
	if( !sendObjectInfo(refArea, lx, ly) )
	{
		cout << "OBJ_TRACKING : Unable to Send Data to UART Interface" << endl;
	} 
#endif
}



void printFilterValues(void)
{
	cout << "HMin = " << H_MIN << "|HMax = " << H_MAX;
	cout << "|SMin = " << S_MIN << "|SMax = " << S_MAX;
	cout << "|VMin = " << V_MIN << "|VMax = " << V_MAX;
	cout << "|Area = " << refArea << "| x = " << x << "|y = " << y;
	cout << endl;

	return;
}



bool loadFilterFromLUT(unsigned char ch)
{
	cout << "Not Supported" << endl;

	return false;
}

unsigned int getDepthZoneFromArea(unsigned int lArea)
{
	unsigned int lDepthZone = ZONE_DEPTH_INVALID;

	if( ( lArea >= DTHRESH_NO_OBJ ) && ( lArea < DTHRESH_MIN ) )
	{
		lDepthZone = ZONE_NO_OBJ;
	}

	else if( (lArea >= DTHRESH_MIN) && ( lArea < DTHRESH_FAR ) )
	{
		lDepthZone = ZONE_FAR;
	}
	else if((lArea >= DTHRESH_FAR) && (lArea < DTHRESH_MID_FAR))
	{
		lDepthZone = ZONE_MID_FAR;
	}
	else if((lArea >= DTHRESH_MID_FAR) && (lArea < DTHRESH_MID))
	{
		lDepthZone = ZONE_MID;
	}
	else if((lArea >= DTHRESH_MID) && (lArea < DTHRESH_MID_NEAR))
	{
		lDepthZone = ZONE_MID_NEAR;
	}
	else if((lArea >= DTHRESH_MID_NEAR) && (lArea < DTHRESH_NEAR ))
	{
		lDepthZone = ZONE_NEAR;
	}
	else if( (lArea >= DTHRESH_NEAR ) )
	{
		lDepthZone = ZONE_NEAR;
	}
	
#if 0
	else if(lArea == DTHRESH_NO_OBJ)
	{
		lDepthZone = ZONE_NO_OBJ;
	}
#endif
	return lDepthZone;
}

unsigned int getLateralPosFromCoOrd(unsigned int lX)
{
	unsigned int lLateralZone = ZONE_LATERAL_INVALID;

	if(lX >= LTHRESH_RIGHT_X)
	{
		lLateralZone = ZONE_RIGHT_X;
	}
	else if((lX < LTHRESH_RIGHT_X) && (lX >= LTHRESH_RIGHT_I))
	{
		lLateralZone = ZONE_RIGHT_I;
	}
	else if((lX < LTHRESH_RIGHT_I) && (lX >= LTHRESH_RIGHT_N))
	{
		lLateralZone = ZONE_RIGHT_N;
	}
	else if((lX < LTHRESH_RIGHT_N) && (lX >= LTHRESH_CENTER))
	{
		lLateralZone = ZONE_CENTER;
	}
	else if((lX < LTHRESH_CENTER) && (lX >= LTHRESH_LEFT_N))
	{
		lLateralZone = ZONE_LEFT_N;
	}
	else if((lX < LTHRESH_LEFT_N) && (lX >= LTHRESH_LEFT_I))
	{
		lLateralZone = ZONE_LEFT_I;
	}
	else if((lX < LTHRESH_LEFT_I) && (lX >= LTHRESH_LEFT_X))
	{
		lLateralZone = ZONE_LEFT_X;
	}

	return lLateralZone;
}

#if UART_ENABLE
bool sendObjectInfo (unsigned int lArea, unsigned int lX, unsigned int lY)
{
	imgSensorInfo imgInfo;

	// Convert Area Values to Depth Zones
	imgInfo.DepthZone = (unsigned char) getDepthZoneFromArea(lArea);

	// Convert X Coordinate to Lateral Position Zones
	imgInfo.LatZone = (unsigned char) getLateralPosFromCoOrd(lX);

	cout << "ImgInfo: LZ = " << (unsigned int)imgInfo.LatZone << " DZ = " << (unsigned int)imgInfo.DepthZone << " A = " << lArea << endl;

	// Post data to the UART Interface Queuea
	return ( img_uart_comm_send(&imgInfo) );

}
#endif

bool openHSVFilterFile(string lFilePath)
{
	filterFileID = open(lFilePath.c_str(), O_RDWR);
	if(filterFileID < 0)
	{
		return false;
	}

	return true;
}

void closeHSVFilterFile(void)
{
	close(filterFileID);
}

bool storeHSVFilterValues(void)
{
	int retval;
	filterValues_t val;

	val.hmin = H_MIN;
	val.hmax = H_MAX;
	val.smin = S_MIN;
	val.smax = S_MAX;
	val.vmin = V_MIN;
	val.vmax = V_MAX;

	retval = write(filterFileID, (char *)&val, sizeof(filterValues_t));
	if(retval != sizeof(filterValues_t))
	{
		cout << "OBJECT_TRACKING : Error writing to File" << endl;
		return false;
	}

	return true;
}	

bool loadHSVFilterValues(void)
{
	int retval;
	filterValues_t val;

	retval = read(filterFileID, (char *)&val, sizeof(filterValues_t));
	if(retval != sizeof(filterValues_t))
	{
		cout << "OBJECT_TRACKING : Error reading from File" << endl;
		return false;
	}

	H_MIN = val.hmin;
	H_MAX = val.hmax; 
	S_MIN = val.smin; 
	S_MAX = val.smax;
	V_MIN = val.vmin;
	V_MAX = val.vmax;

	return true;
}


bool keyPressAction(char ch)
{
	switch(ch)
	{
		case CMD_STORE_FILTER_VALUES:
			
			openHSVFilterFile(filterValueFile);
			storeHSVFilterValues();
			closeHSVFilterFile();

			cout << "OBJECT_TRACKING : HSV Filter Values stored to File!" << endl;

			printFilterValues();

			break;

		case CMD_LOAD_FILTER_VALUES:

			openHSVFilterFile(filterValueFile);
			loadHSVFilterValues();
			closeHSVFilterFile();

			cout << "OBJECT_TRACKING : HSV Filter Value restored from File!" << endl;

			printFilterValues();

			break;

		// H Values
		case CMD_HMIN_INCR:
			H_MIN++;

			if(H_MIN > MAXVAL)
			{
				H_MIN = MAXVAL;
			}

			printFilterValues();

			break;

		case CMD_HMIN_DECR:
			H_MIN--;

			if(H_MIN < 0)
			{
				H_MIN = 0;
			}

			printFilterValues();

			break;

		case CMD_HMAX_INCR:
			H_MAX++;

			if(H_MAX > MAXVAL)
			{
				H_MAX = MAXVAL;
			}

			printFilterValues();
			
			break;

		case CMD_HMAX_DECR:
			H_MAX--;

			if(H_MAX < 0)
			{
				H_MAX = 0;
			}

			printFilterValues();

			break;

		// S Value
		case CMD_SMIN_INCR:
			S_MIN++;

			if(S_MIN > MAXVAL)
			{
				S_MIN = MAXVAL;
			}

			printFilterValues();

			break;

		case CMD_SMIN_DECR:
			S_MIN--;

			if(S_MIN < 0)
			{
				S_MIN = 0;
			}
			
			printFilterValues();

			break;

		case CMD_SMAX_INCR:
			S_MAX++;

			if(S_MAX > MAXVAL)
			{
				S_MAX = MAXVAL;
			}

			printFilterValues();

			break;

		case CMD_SMAX_DECR:
			S_MAX--;

			if(S_MAX < 0)
			{
				S_MAX = 0;
			}

			printFilterValues();

			break;

		// Intensity
		case CMD_VMIN_INCR:
			V_MIN++;

			if(V_MIN > MAXVAL)
			{
				V_MIN = MAXVAL;
			}
			
			printFilterValues();

			break;

		case CMD_VMIN_DECR:
			V_MIN--;

			if(V_MIN < 0)
			{
				V_MIN = 0;
			}

			printFilterValues();

			break;

		case CMD_VMAX_INCR:
			V_MAX++;

			if(V_MAX > MAXVAL)
			{
				V_MAX = MAXVAL;
			}
			
			printFilterValues();

			break;

		case CMD_VMAX_DECR:
			V_MAX--;

			if(V_MAX < 0)
			{
				V_MAX = 0;
			}

			printFilterValues();

			break;

		case CMD_RESET_FITLER:
			H_MIN = 0;
			H_MAX = 255;
			S_MIN = 0;
			S_MAX = 255;
			V_MIN = 0;
			V_MAX = 255;
			printFilterValues();

			break;

		case CMD_WINDOW_TH_IMAGE:
			
			if(thresholdWindowStatus)
			{
				cout << "OBJECT_TRACKING : Creating Threshold Window" << endl;
 			}

			thresholdWindowStatus = !thresholdWindowStatus;
			break;

		case CMD_ESC:
			return false;
	}

	return true;
}

void raiseTaskPriority(void)
{
	static int priority = 98;
	struct sched_param sparams = {.sched_priority = priority};
	int taskPID = getpid();

	if( sched_setscheduler(taskPID, SCHED_FIFO, &sparams) != 0)
	{
		cout << "OBJECT_TRACKING : Error setting Task Prority" << endl;
		exit(0);
	}

	return;
}

int main(int argc, char* argv[])
{
	//some boolean variables for different functionality within this
	//program
    unsigned int camId = 0;
    char ch;

	//Matrix to store each frame of the webcam feed
	Mat cameraFeed;

	//matrix storage for HSV image
	Mat HSV;

	//matrix storage for binary threshold image
	Mat threshold;

	raiseTaskPriority();

#if TRACKBAR
	//create slider bars for HSV filtering
	createTrackbars();
#endif

	//video capture object to acquire webcam feed
	VideoCapture capture;
	//open capture object at location zero (default location for webcam)

	if(argc != 3)
	{
		cout << "./object_tracking <CamID> <FilterFilePath>" << endl;
		exit(0);
	}

	camId = atoi(argv[1]);
	filterValueFile = argv[2];

	cout << "CAM id = " << camId << endl;
	cout << "FilterValue File = " << filterValueFile << endl;

	if ( !openHSVFilterFile(filterValueFile) )
	{
		cout << "HSV Filter File Open Failed!" << endl;
		exit(0);
	}

#if UART_ENABLE
	char uartDevPath[] = UART_DEV_PATH;
	if ( !img_uart_comm_init(uartDevPath) )
	{
		cerr << "OBJECT_TRACKING : Uart Init Failed" << endl;
		exit(0);
	}
#endif

	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
	capture.set(CV_CAP_PROP_FPS, FRAME_RATE);

	capture.open(camId);

	//start an infinite loop where webcam feed is copied to cameraFeed matrix
	//all of our operations will be performed within this loop

	namedWindow(ActualWindow, 0);
	moveWindow(ActualWindow, 0, 0);

	namedWindow(ThresholdWindow, 0);
	moveWindow(ThresholdWindow, 320, 0);

	#if HSV_WINDOW
		namedWindow(HSVImageWindow, 0);
	#endif

	while(1)
	{
		//store image to matrix
		capture.read(cameraFeed);

		//convert frame from BGR to HSV colorspace
		cvtColor(cameraFeed, HSV, COLOR_BGR2HSV);

		//filter HSV image between values and store filtered image to
		//threshold matrix
		inRange(HSV, Scalar(H_MIN,S_MIN,V_MIN), Scalar(H_MAX,S_MAX,V_MAX), threshold);

		//perform morphological operations on thresholded image to eliminate noise
		//and emphasize the filtered object(s)
		if(useMorphOps)
		{
			morphOps(threshold);
		}

		//pass in thresholded frame to our object tracking function
		//this function will return the x and y coordinates of the
		//filtered object
		if(trackObjects)
		{
			trackFilteredObject(x,y,threshold,cameraFeed);
		}

		//show frames 
		imshow(ActualWindow, cameraFeed);
		imshow(ThresholdWindow, threshold);

		#if HSV_WINDOW
			imshow(HSVImageWindow, HSV);
		#endif

		//delay 30ms so that screen can refresh.
		//image will not appear without this waitKey() command
		ch = waitKey(30);
		if( !keyPressAction(ch) )
		{
			break;
		}
	}

#if UART_ENABLE
	img_uart_comm_exit();
#endif

	return 0;
}

