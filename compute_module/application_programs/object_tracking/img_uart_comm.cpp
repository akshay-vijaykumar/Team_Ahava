

#include <iostream>
#include <pthread.h>
#include <mqueue.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#include "img_uart_comm.h"

using namespace std;

#if 0
extern int H_MIN;
extern int H_MAX;
extern int S_MIN;
extern int S_MAX;
extern int V_MIN;
extern int V_MAX;
#endif

#define USE_WRITE_Q		0
#define USE_READ_Q		1	

bool keyPressAction(char ch);
bool loadFilterFromLUT(unsigned char ch);

// UART Device Info
int uartFD;

// Message Queue ID
#define MAX_MSG_COUNT ( 3 )
#define QUEUE_NAME "/imgwritequeue"

mqd_t writeQueueID;
const char * writeQueueName = QUEUE_NAME;
struct mq_attr writeQueuAttr;
unsigned int key = 24282;

// Thread ID's
static pthread_t readThreadID;
static pthread_t writeThreadID;

// Thread Functions

/* img_uart_write_thread
 * Desc : Thread that Reads from UART
 */

#if USE_READ_Q
void * img_uart_read_thread(void *)
{
	btData btCommandData;

	while(1)
	{
		// Check for new data on UART
		img_uart_comm_recv(&btCommandData);

		// Decoded message is obtained. Action on this message is taken
		if(btCommandData.command == COMMAND_LIGHT_SENSOR)
		{
			loadFilterFromLUT(btCommandData.lightSensor);
		}
		else if(btCommandData.command == COMMAND_THRESHOLD_FILTER)
		{
			keyPressAction(btCommandData.threshData);
		}
	}

	return 0;
}
#endif

/* img_uart_write_thread
 * Desc : Thread that Writes to UART
 */
 #if USE_WRITE_Q
void * img_uart_write_thread(void *)
{
	imgSensorInfo sensorInfo;
	int retval = -1;

	while(1)
	{
		retval = mq_receive(writeQueueID, (char *)&sensorInfo, sizeof(imgSensorInfo), NULL);
		if( retval < 0 )
		{
			cerr << "IMG_UART_COMM : Error in receiving imgSensorInfo from queue\n";
			continue;
		}

		cout << "IMG_UART_COMM : Dequeued message. Writing to UART\n";

		retval = write( uartFD, (char *)&sensorInfo, sizeof(imgSensorInfo));
		if( retval != sizeof(imgSensorInfo))
		{
			cerr << "IMG_UART_COMM : Error in sending imgSensorInfo on the uart\n";
		}
	}

	return 0;
}
#endif

/* img_uart_comm_send
 * Desc    : Function that would send data to UART Communication Interface
 * @Params : 
 */
bool img_uart_comm_send(imgSensorInfo* data)
{
	int retval = -1;
	
	// Put a sync byte in place
	data->SyncByte = SINFO_SYNC_BYTE;
	
	// Queue the data
#if USE_WRITE_Q
	retval = mq_send(writeQueueID, (char *)data, sizeof(imgSensorInfo), 0);
	if( retval < 0 )
	{
		cout << "IMG_UART_COMM : Error in queuing image sensor data\n";
		return false;
	}

#else
	retval = write( uartFD, (char *)data, sizeof(imgSensorInfo));

	if( retval != sizeof(imgSensorInfo))
	{
		cout << "IMG_UART_COMM : Error in sending imgSensorInfo on the uart\n";
		close(uartFD);
		return false;
	}

//	cout << "Sent image data: " << (unsigned int)data->LatZone << " " << (unsigned int)data->DepthZone << endl;

#endif

	return true;
}

/* img_uart_comm_recv
 * Desc : Function that would receive the UART Message and fill in an instance
 */ 
void img_uart_comm_recv(btData* data)
{
	int temp = 0;

	// Receive from UART
	temp = read(uartFD, (char *)data, sizeof(btData));

	if( temp != sizeof(btData))
	{
		cout << "IMG_UART_COMM : Error in receiving bluetooth data over the uart\n";
	}

	else
	{
		cout << "IMG_UART_COMM : Received data from UART : command :: " << (unsigned int)data->command << " lightSensor :: " 
		 << (unsigned int)data->lightSensor << " threshData :: " << (unsigned int)data->threshData << endl;
	}


	return;
}

/*
 *	Image Sensor UART Communication Initialization
 */
bool img_uart_comm_init(char * lDevPath)
{
	//Open UART Device
	uartFD = open(lDevPath, O_RDWR);
	if(uartFD < 0)
	{
		cout << "IMG_UART_COMM : Error in Opening UART Device" << endl;
		return false;
	}

#if USE_WRITE_Q
	// Set up Write Queue
	writeQueuAttr.mq_flags = 0; // Blocking Read Write Queue
	writeQueuAttr.mq_maxmsg = MAX_MSG_COUNT; // Max number of messages allowed
	writeQueuAttr.mq_msgsize = sizeof(imgSensorInfo); // Size of Element being sent on the queue
	writeQueuAttr.mq_curmsgs = 0; // Number of messages currently in queue

	writeQueueID = mq_open(writeQueueName, O_CREAT | O_RDWR, 0644, &writeQueuAttr);	
	if(writeQueueID < 0)
	{
		cout << "IMG_UART_COMM : Error in Creating Write Queue" << endl;
		return false;
	}
#endif

	// We dont have a read queue. 
	// Create the Read and Write Thread
#if USE_READ_Q
	if( pthread_create( &readThreadID, NULL, img_uart_read_thread, NULL) )
	{
		cout << "IMG_UART_COMM : Error in Creating Read Thread" << endl;
		return false;
	}
#endif

#if USE_WRITE_Q
	if( pthread_create( &writeThreadID, NULL, img_uart_write_thread, NULL) )
	{
		cout << "IMG_UART_COMM : Error in Creating Write Thread" << endl;
		return false;
	}
#endif


	return true;
}

/* 
 *	Image Sensor UART Communication Exit Function
 */
void img_uart_comm_exit(void)
{
	// Terminate All threads

	// Close UART Device
	close(uartFD);

	// Destroy Write Queue
	mq_close(writeQueueID);
	mq_unlink(writeQueueName);

	return;
}