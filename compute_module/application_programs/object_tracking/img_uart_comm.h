#ifndef __IMG_UART_COMM_H
#define __IMG_UART_COMM_H

/*
 *	Distance thresholds for different objDepthZones
 */

#define DTHRESH_NO_OBJ 		( 0 )
#define DTHRESH_MIN 		( 400 )
#define DTHRESH_FAR         ( 500  )
#define DTHRESH_MID_FAR     ( 1000 )
#define DTHRESH_MID         ( 3500 )
#define DTHRESH_MID_NEAR    ( 11500 )
#define DTHRESH_NEAR        ( 15000 )
 
/*
 *	Distance thresholds for different objLatZones - in terms of Pixel Values
 */

#define LTHRESH_LEFT_X     ( 0   )
#define LTHRESH_LEFT_I     ( 90  )
#define LTHRESH_LEFT_N     ( 180 )
#define LTHRESH_CENTER     ( 270 )
#define LTHRESH_RIGHT_N    ( 360 )
#define LTHRESH_RIGHT_I    ( 450 )
#define LTHRESH_RIGHT_X    ( 540 )

#define SINFO_SYNC_BYTE	   ( 0xE5 )

// Type defines
typedef unsigned int uint32_t;	
typedef unsigned char uint8_t;

typedef uint8_t imgSInfo;


typedef struct monoObjPos		// Tracking data from a single camera
{
	uint32_t objArea;			// Area occupied by the object
	uint32_t objXPos;			// X co-ordinate of the object
	uint32_t objYPos;			// Y co-ordinate of the object
	void*	      objThresh;			// Thresholding information used for this object

} mObjPos;

typedef struct objHSVThresh		// Thresholding information for an object in HSV space
{
	uint8_t objHueThresh;		// Hue threshold for tracking this object
	uint8_t objSatThresh;		// Saturation threshold for tracking this object
	uint8_t objValThresh;		// Value threshold for tracking this object
	uint8_t reserved;			// For future use

} objHSVThresh;

typedef enum objLatZones	// Lateral zone in which the object is located
{
	ZONE_LEFT_X,		// Object is to the extreme left of the frame
	ZONE_LEFT_I,			// Object is to the intermediate left of the frame
	ZONE_LEFT_N,		// Object is to the near left of the frame
	ZONE_CENTER,		// Object is to the near left of the frame
	ZONE_RIGHT_N,		// Object is to the near right of the frame
	ZONE_RIGHT_I,		// Object is to the near right of the frame
	ZONE_RIGHT_X,		// Object is to the near right of the frame
	ZONE_LATERAL_INVALID

} objLZones;

typedef enum objDepthZones	// Depth zones in which object is located
{
	ZONE_NO_OBJ,
	ZONE_FAR,
	ZONE_MID_FAR,
	ZONE_MID,
	ZONE_MID_NEAR,
	ZONE_NEAR,
	ZONE_DEPTH_INVALID

} objDZones;


typedef struct imgSensorInfo
{

	imgSInfo	SyncByte;
	imgSInfo	LatZone;
	imgSInfo	DepthZone;
	imgSInfo	Reserved2;

} imgSensorInfo;

typedef enum objThreshModifier
{
	HPLUS,
	HMINUS,
	SPLUS,
	SMINUS,
	VPLUS,
	VMINUS,
	NOP
		
}  objThreshMod;

typedef enum btCommand 
{
	COMMAND_LIGHT_SENSOR,
	COMMAND_THRESHOLD_FILTER,
	COMMAND_END
} btCommand;

typedef struct btData
{
	uint8_t command;
	uint8_t lightSensor;
	uint8_t threshData;
	uint8_t Reserved1;	

} btData;


// Function Prototypes
void img_uart_comm_recv(btData* data);
bool img_uart_comm_send(imgSensorInfo* data);

bool img_uart_comm_init(char * lDevPath);
void img_uart_comm_exit(void);

#endif