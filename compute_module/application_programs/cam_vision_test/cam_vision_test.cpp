#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
//#include "opencv2/videoio.hpp"
#include <iostream>

using namespace cv;
using namespace std;

void drawText(Mat & cam0Image);

int main(int argc, char* argv[])
{

    Mat cam0Image;

    VideoCapture cam0;

    bool cam0Status = false;
    
    cout << "CAM Video Streaming Application" << endl;

	if( (argv[1] == NULL))
	{
		cout << "Error - Enter Camera Index" << endl;
		return 0;
	}

	int cam0Index = atoi(argv[1]);

	cam0.open(cam0Index);
    
	if(cam0.isOpened())
    {
        cout << "cam0 is opened! - Index = " <<  cam0Index << endl;
        cam0Status = true;
    }
    else
    {
        cout << "Error : cam0 not connected" << endl;
    }

	cam0 >> cam0Image;
	
	/* Creating and Positioning Cam Windows */

	const char* CAM0_WINDOW_NAME = "CAM0";

	namedWindow(CAM0_WINDOW_NAME, CV_WINDOW_AUTOSIZE);
	
	moveWindow(CAM0_WINDOW_NAME, 0, 0);

	cout << "cam0 : [row,col] = [" << cam0Image.rows << ", " << cam0Image.cols << "]" << endl;

    if(cam0Status)
    {
        for(;;)
        {
            cam0 >> cam0Image;

            if(cam0Image.empty())
                break;

            //drawText(cam0Image);
            imshow(CAM0_WINDOW_NAME, cam0Image);

            if(waitKey(10) >= 0)
              break;
        }   
    }

    return 0;
}

