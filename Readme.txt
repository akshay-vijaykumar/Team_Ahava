======================================================================================

Mobile object detection and tracking using computer vision

======================================================================================


This project is an effort to build a computer vision system that can detect and track
the required objects on a mobile platform, which can follow the target.

The source code is divided into two parts.

1. Compute module - Software components for the Raspberry Pi Compute module can be located here.
2. lpc1758_freertos - Software components for the lpc micro-controller can be located here.

The role of the compute module is to perform the required image processing and provide directions
to the lpc micro-controller. The micro-controller then drives the car as directed.

The compute module is based on the buildroot utility. The imaging is performed using openCV and the
camera interfaces provided on the module. To see the build configuration for the compute module
cd into "compute_module/bcm_usbboot/buildroot/" and execute 

make menuconfig

Go into each of the component directories to find other "readme" files.
